import { Component } from '@angular/core';
import { Observable, of } from 'rxjs';
import { IChangeLogInfo } from 'bb-itag-angular-change-log';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-my-change-log',
  templateUrl: './my-change-log.component.html',
  styleUrls: ['./my-change-log.component.scss']
})
export class MyChangeLogComponent {

  public changeLogInfos: IChangeLogInfo[];

  constructor(
    private httpClient: HttpClient,
  ) {
    this.loadChangeLog()
      .subscribe((value) => {
        this.changeLogInfos = value;
      });
  }
  public loadChangeLog(): Observable<IChangeLogInfo[]> {
    return this.httpClient.get<IChangeLogInfo[]>('/assets/config/changeLog.json');
  }

}
